﻿using System;
using System.Linq;
using UnityEngine;

public abstract class Creature : MonoBehaviour
{
    #region Declarations
    [SerializeField] protected Transform groundCheck;
    [SerializeField] protected LayerMask groundMask;
    [SerializeField] protected int maxHp;

    [HideInInspector] public int currentHp;

    protected RaycastHit hit;
    protected enum State { Idle, Walking, Shooting, Melee, Reloading, Interacting };
    protected const float gravAcceleration = -9.81f;
    protected State state = State.Idle;
    protected float groundDistance = 0.5f;
    protected bool isGrounded;
    protected bool isReloading;
    protected bool canShoot;
    protected bool canMelee;
    #endregion

    private void Awake()
    {
        currentHp = maxHp;
    }

    public void TakeDamage(int damage)
    {
        currentHp -= damage;
        Debug.LogWarning(currentHp);
        if (currentHp <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        Destroy(gameObject);
    }

    protected void GroundCheck()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
    }

    public void LockRotationX()
    {
        this.gameObject.transform.localEulerAngles = new Vector3(0, this.gameObject.transform.localEulerAngles.y, this.gameObject.transform.localEulerAngles.z);
    }
}
