﻿using UnityEngine;

public class Inventory : MonoBehaviour
{
    #region Declarations
    [SerializeField] private GameObject inventory = null;

    private bool inventoryOpened = false;
    #endregion

    public void OpenInventory()
    {
        Time.timeScale = 1 - Time.timeScale;
        inventoryOpened = !inventoryOpened;
        inventory.SetActive(inventoryOpened);
    }
}
