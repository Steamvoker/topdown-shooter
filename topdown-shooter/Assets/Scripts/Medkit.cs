﻿public class Medkit : Item
{
    public override void Pickup(Player playerComponent)
    {
        playerComponent.TakeDamage(-itemQuantity);
        base.Pickup(playerComponent);
    }
}
