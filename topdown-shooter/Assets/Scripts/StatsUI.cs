﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StatsUI : MonoBehaviour
{
    #region Declarations
    [SerializeField] private TMP_Text ammoText = null;
    [SerializeField] private TMP_Text healthText = null;
    [SerializeField] private GunController gunController = null;
    [SerializeField] private Slider healthSlider = null;
    [SerializeField] private Slider hungerSlider = null;
    [SerializeField] private Slider thirstSlider = null;
    [SerializeField] private Player player = null;
    #endregion
    private void Start()
    {
        player = FindObjectOfType<Player>();
        healthSlider.maxValue = player.currentHp;
        hungerSlider.maxValue = player.currentHunger;
        thirstSlider.maxValue = player.currentThirst;
    }

    private void Update()
    {
        if (player != null)
            gunController = player.GetComponentInChildren<GunController>();

        ammoText.text = gunController.currentLoadedAmmo.ToString() + " / " + gunController.currentCarryAmmo.ToString();
        healthText.text = player.currentHp.ToString();
        SetHealth(player.currentHp);
        SetHunger(player.currentHunger);
        SetThirst(player.currentThirst);
    }

    public void SetHealth(int health)
    {
        healthSlider.value = health;
    }

    public void SetHunger(int hunger)
    {
        hungerSlider.value = hunger;
    }

    public void SetThirst(int thirst)
    {
        thirstSlider.value = thirst;
    }
}
