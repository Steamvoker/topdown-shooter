﻿using System.Collections;
using UnityEngine;

public class GunController : MonoBehaviour
{
    #region Declarations
    [SerializeField] private Gun gun = null;
    [SerializeField] private InputManager inputManager = null;

    [HideInInspector] public int currentLoadedAmmo;
    [HideInInspector] public int currentCarryAmmo;
    [HideInInspector] public bool isReloading = false;
    [HideInInspector] public float nextShotTime = 0f;

    private bool canMelee = true;
    private RaycastHit hit;
    private int maxCarryCapacity;
    #endregion

    private void Awake()
    {
        inputManager = FindObjectOfType<InputManager>();
    }

    private void Start()
    {
        Mathf.Clamp(currentLoadedAmmo, 0, gun.ammoCapacity);
        currentLoadedAmmo = gun.ammoCapacity;
        maxCarryCapacity = currentLoadedAmmo * 10;
        Mathf.Clamp(currentCarryAmmo, 0, maxCarryCapacity);
        currentCarryAmmo = currentLoadedAmmo * 2;
    }

    private void Update()
    {
        PerformReload();
    }

    /// <summary>Reloads ammo from carry to the gun.</summary>
    public IEnumerator Reload()
    {
        if (currentCarryAmmo > 0)
        {
            isReloading = true;
            yield return new WaitForSecondsRealtime(gun.reloadSpeed);
            currentLoadedAmmo = gun.ammoCapacity;
            currentCarryAmmo -= currentLoadedAmmo;
            isReloading = false;
        }
    }

    public void PerformReload()
    {
        if (isReloading)
        {
            return;
        }

        if (currentLoadedAmmo == 0 && currentLoadedAmmo != gun.ammoCapacity)
        {
            StartCoroutine(Reload());
            return;
        }
    }

    public void PerformMelee()
    {
        if (Physics.Raycast(transform.position, transform.forward, out hit, gun.range) && canMelee)
        {
            Creature target = hit.transform.GetComponent<Creature>();
            {
                if (target != null)
                {
                    target.TakeDamage(gun.damage);
                    Knockback(gun);
                }
            }
            canMelee = false;
        }
        canMelee = true;
    }

    protected void Knockback(Gun gun)
    {
        if (hit.rigidbody != null)
        {
            hit.rigidbody.AddForce(-hit.normal * gun.knockbackStrength, ForceMode.Impulse);
        }
    }

    public void PerfShot()
    {
        if (Physics.Raycast(transform.position, transform.forward, out hit, gun.range) && currentLoadedAmmo != 0 && !isReloading)
        {
            currentLoadedAmmo--;
            Creature target = hit.transform.GetComponent<Creature>();
            if (target != null)
            {
                target.TakeDamage(gun.damage);
                Knockback(gun);
            }
        }
    }
}
