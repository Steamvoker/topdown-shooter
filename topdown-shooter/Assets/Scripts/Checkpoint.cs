﻿using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    #region Declarations
    [SerializeField] private GameObject spawnLocation = null;
    [SerializeField] private Player player;
    private GameMaster gameMaster;
    #endregion

    private void Start()
    {
        gameMaster = GameObject.FindGameObjectWithTag("Game Master").GetComponent<GameMaster>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            gameMaster.currentCheckpointPosition = spawnLocation.transform.position;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(new Vector3(spawnLocation.transform.position.x, spawnLocation.transform.position.y + 1.5f, 
            spawnLocation.transform.position.z), new Vector3(1, 3, 1));
    }
}
