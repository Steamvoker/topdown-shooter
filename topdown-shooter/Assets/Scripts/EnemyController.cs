﻿using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    #region Declarations
    [SerializeField] private Enemy enemy = null;
    [SerializeField] private GunController gunController = null;
    [SerializeField] private NavMeshAgent agent;
    [SerializeField] private Gun gun = null;

    private NavMeshPath path;
    private Vector3 target;
    #endregion

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    private void Update()
    {
        if (!agent.SetDestination(target))
        {
            // Wander around 
        }

        MoveToPlayer();

        if (agent.remainingDistance <= gun.range)
        {
            if (gun.fireMode == Gun.FireMode.Semi && Time.time >= gunController.nextShotTime)
            {
                gunController.nextShotTime = Time.time + 60f / gun.fireRate;
                gunController.PerfShot();
            }
            else if (gun.fireMode == Gun.FireMode.Melee && Time.time >= gunController.nextShotTime)
            {
                gunController.nextShotTime = Time.time + 60f / gun.fireRate;
                gunController.PerformMelee();
            }
        }
    }

    private void MoveToPlayer()
    {
        path = new NavMeshPath();
        if (GameObject.FindGameObjectWithTag("Player") != null)
            target = GameObject.FindGameObjectWithTag("Player").transform.position;

        agent.CalculatePath(target, path);
        if (path.status == NavMeshPathStatus.PathComplete)
        {
            if (agent.remainingDistance <= enemy.chaseDistance)
            {
                agent.isStopped = false;
                agent.SetDestination(target);
                FacePlayer();
            }
            else if (agent.remainingDistance >= enemy.chaseDistance)
            {
                agent.isStopped = true;
            }
        }
    }

    private void FacePlayer()
    {
        transform.LookAt(new Vector3(target.x, this.gameObject.transform.position.y, target.z));
    }
}
