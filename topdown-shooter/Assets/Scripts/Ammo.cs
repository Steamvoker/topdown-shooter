﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammo : Item
{
    //#region Declarations
    //[SerializeField] private AmmoType ammoType;
    //#endregion

    //public enum AmmoType
    //{
    //    NineMil, FiveFiveSix, SevenSixTwo, TwelveGauge
    //}
    
    public override void Pickup(Player playerComponent)
    {
        playerComponent.AddAmmo(itemQuantity);
        base.Pickup(playerComponent);
    }
}
