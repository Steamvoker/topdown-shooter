﻿using UnityEngine;

public class GameMaster : MonoBehaviour
{
    #region Declarations
    public static GameMaster instance;
    public Vector3 currentCheckpointPosition;
    #endregion

    private void Awake()
    {

        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
