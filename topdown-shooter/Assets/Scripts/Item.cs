﻿using UnityEngine;

public class Item : MonoBehaviour
{
    #region Declarations
    [SerializeField] protected Sprite icon;
    [SerializeField] protected string itemName = null;
    [SerializeField] protected string itemDesciption = null;
    [SerializeField] protected int itemQuantity = 1;
    #endregion

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Player playerComponent = other.GetComponent<Player>();
            Pickup(playerComponent);
        }
    }

    public virtual void Pickup(Player playerComponent)
    {
        Destroy(this.gameObject);
    }
}
