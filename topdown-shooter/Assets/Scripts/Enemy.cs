﻿using UnityEngine;

public abstract class Enemy : Creature
{
    #region Declarations
    public float chaseDistance;
    public float attackRange;
    #endregion

    private void Start()
    {
        Mathf.Clamp(currentHp, 0, maxHp);
        currentHp = maxHp;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, chaseDistance);
    }
}
