﻿using UnityEngine;

public class Gun : MonoBehaviour
{
    #region Declarations
    public enum FireMode
    {
        Semi, Auto, Burst, Melee
    };
    public FireMode fireMode;
    public int fireRate = 120;
    public int burstRate = 2;
    public float reloadSpeed = 3f;
    public int ammoCapacity = 8;
    public int damage = 20;
    public float range = 30f;
    public float knockbackStrength = 5f;
    #endregion
}
