﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Exit : MonoBehaviour
{
    [SerializeField] private MenuUI menuUI = null;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
           menuUI.WinMenu();
    }
}
