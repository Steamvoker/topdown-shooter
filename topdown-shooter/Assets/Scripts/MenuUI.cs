﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuUI : MonoBehaviour
{
    #region Declarations
    [SerializeField] private GameObject pauseMenu = null;
    [SerializeField] private GameObject gameOverMenu = null;
    [SerializeField] private GameObject winMenu = null;
    [SerializeField] private GameMaster gameMaster;
    [SerializeField] private Player player = null;

    private bool hasRun = false;
    #endregion

    private void Update()
    {
        if (player.currentHp <= 0 && hasRun == false)
        {
            GameOverMenu();
            hasRun = !hasRun;
        }
    }

    public void PauseToggle()
    {
        Time.timeScale = 1 - Time.timeScale;
        pauseMenu.SetActive(!pauseMenu.activeSelf);
    }

    public void MainMenu()
    {
        Time.timeScale = 1 - Time.timeScale;
        SceneManager.LoadScene(0);
    }

    public void Restart()
    {
        Time.timeScale = 1 - Time.timeScale;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void GameOverMenu()
    {
        Time.timeScale = 1 - Time.timeScale;
        gameOverMenu.SetActive(!gameOverMenu.activeSelf);
    }

    public void WinMenu()
    {
        Time.timeScale = 1 - Time.timeScale;
        winMenu.SetActive(!winMenu.activeSelf);
    }
}
