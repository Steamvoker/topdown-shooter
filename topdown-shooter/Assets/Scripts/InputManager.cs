﻿using UnityEngine;
public class InputManager : MonoBehaviour
{
    #region Declarations
    //[SerializeField] private Inventory inventory = null;
    [SerializeField] private Player player = null;
    [SerializeField] private Gun gun = null;
    [SerializeField] private GunController gunController = null;
    [SerializeField] private MenuUI menuUI = null;
    [SerializeField] private GunSwitch gunSwitch = null;

    [HideInInspector] public Vector2 move;

    private InputMaster playerInput;
    #endregion

    private void Awake()
    {
        player = FindObjectOfType<Player>();

        playerInput = new InputMaster();

        playerInput.Player.Movement.performed += _ => move = _.ReadValue<Vector2>();
        playerInput.Player.Reload.performed += _ => StartCoroutine(gunController.Reload());
        //playerInput.Player.OpenInventory.performed += _ => inventory.OpenInventory();
        playerInput.Player.PauseStart.performed += _ => menuUI.PauseToggle();
    }

    private void Update()
    {
        gunController = player.GetComponentInChildren<GunController>();
        gun = player.GetComponentInChildren<Gun>();
        WeaponSwitch();
        Shoot();
    }

    private void WeaponSwitch()
    {
        if (Input.GetAxis("Mouse ScrollWheel") > 0f)
        {
            if (gunSwitch.selectedWeapon >= gunSwitch.transform.childCount - 1)
                gunSwitch.selectedWeapon = 0;
            else
                gunSwitch.selectedWeapon++;
            gunSwitch.SelectWeapon();
        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0f)
        {
            if (gunSwitch.selectedWeapon <= 0)
                gunSwitch.selectedWeapon = gunSwitch.transform.childCount - 1;
            else
                gunSwitch.selectedWeapon--;
            gunSwitch.SelectWeapon();
        }
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            gunSwitch.selectedWeapon = 0;
            gunSwitch.SelectWeapon();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            gunSwitch.selectedWeapon = 1;
            gunSwitch.SelectWeapon();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            gunSwitch.selectedWeapon = 2;
            gunSwitch.SelectWeapon();
        }
        //else if (Input.GetKeyDown(KeyCode.Alpha4))
        //{
        //    gunSwitch.selectedWeapon = 3;
        //    gunSwitch.SelectWeapon();
        //}
    }

    void Shoot()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0) && gun.fireMode == Gun.FireMode.Semi && Time.time >= gunController.nextShotTime)
        {
            gunController.nextShotTime = Time.time + 60f / gun.fireRate;
            gunController.PerfShot();
        }
        else if (Input.GetKey(KeyCode.Mouse0) && gun.fireMode == Gun.FireMode.Auto && Time.time >= gunController.nextShotTime)
        {
            gunController.nextShotTime = Time.time + 60f / gun.fireRate;
            gunController.PerfShot();
        }
        else if (Input.GetKeyDown(KeyCode.Mouse0) && gun.fireMode == Gun.FireMode.Burst && Time.time >= gunController.nextShotTime)
        {
            gunController.nextShotTime = Time.time + 60f / gun.fireRate;
            gunController.PerfShot();
        }
        else if (Input.GetKeyDown(KeyCode.Mouse0) && gun.fireMode == Gun.FireMode.Melee && Time.time >= gunController.nextShotTime)
        {
            gunController.nextShotTime = Time.time + 60f / gun.fireRate;
            gunController.PerformMelee();
        }
    }

    private void OnEnable()
    {
        playerInput.Player.Enable();
    }

    private void OnDisable()
    {
        playerInput.Player.Disable();
    }
}
