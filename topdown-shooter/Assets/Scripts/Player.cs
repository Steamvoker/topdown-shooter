﻿using UnityEngine;

public class Player : Creature
{
    #region Declarations
    [SerializeField] protected float movementSpeed;
    [SerializeField] private int maxThirst = 0;
    [SerializeField] private int maxHunger = 0;
    [SerializeField] private GunController gunController = null;
    [SerializeField] private CharacterController characterController = null;

    [HideInInspector] public Vector3 mousePos;
    [HideInInspector] public int currentThirst;
    [HideInInspector] public int currentHunger;

    private InputManager inputManager;
    private Vector3 verticalForce;
    private GameMaster gameMaster;
    #endregion

    private void Awake()
    {
        currentThirst = maxThirst;
        currentHunger = maxHunger;
        inputManager = FindObjectOfType<InputManager>();
    }

    private void Start()
    {
        gameMaster = GameObject.FindGameObjectWithTag("Game Master").GetComponent<GameMaster>();
        transform.position = gameMaster.currentCheckpointPosition;
    }

    private void Update()
    {
        gunController = GetComponentInChildren<GunController>();
        Move();
        GroundCheck();
        LockRotationX();
    }

    private void FixedUpdate()
    {
        LookAtMouse();
    }

    public void Move()
    {
        state = State.Walking;
        if (isGrounded && verticalForce.y < 0)
        {
            verticalForce.y = -2f;
        }
        verticalForce.y += gravAcceleration * Time.deltaTime;
        var move = new Vector3(inputManager.move.x * movementSpeed * Time.deltaTime,
                                verticalForce.y * Time.deltaTime, 
                                inputManager.move.y * movementSpeed * Time.deltaTime);
        characterController.Move(move);
    }

    private void LookAtMouse()
    {
        int cameraHeight = (int)Camera.main.gameObject.transform.position.y;
        mousePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, cameraHeight));
        transform.LookAt(mousePos);
    }

    public void AddAmmo(int amount)
    {
        gunController.currentCarryAmmo += amount;
    }
}
