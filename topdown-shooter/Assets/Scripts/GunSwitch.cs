﻿using UnityEngine;

public class GunSwitch : MonoBehaviour
{
    [HideInInspector] public int selectedWeapon = 0;

    private void Start()
    {
        SelectWeapon();
    }

    public void SelectWeapon()
    {
        int index = 0;
        foreach (Transform weapon in transform)
        {
            if (index == selectedWeapon)
                weapon.gameObject.SetActive(true);
            else
                weapon.gameObject.SetActive(false);
            index++;
        }
    }
}
